function loopImgs() {
    let wrapper = document.querySelector('.images-wrapper');
    let images = document.querySelectorAll('img');
    let stopButton = document.createElement('button');
    let continueButton = document.createElement('button');

    stopButton.innerText = 'Прекратить';
    continueButton.innerText = 'Возобновить показ';
    stopButton.style.marginBottom = '20px';
    wrapper.style.position = 'relative';
    document.body.prepend(stopButton);
    stopButton.after(continueButton);
    
    let currentIndex = 0;

    images.forEach(item => {
        item.style.cssText = "position: absolute; display: none; top: 0; left: 0";
    })

    images[0].style.display = 'block';

    let working = false;

        const changeImg = () => {
            working = true;
            images[currentIndex].style.display = 'none';

            if (currentIndex === images.length - 1) {
                currentIndex = 0;
            } else {
                currentIndex++;
            }
            images[currentIndex].style.display = 'block';
        };

    let changeImgBegin = setInterval(changeImg, 3000);

        continueButton.addEventListener('click', () => {
            if(!working) changeImgBegin = setInterval(changeImg, 3000);
        });

        stopButton.addEventListener('click', () => {
            clearInterval(changeImgBegin);
            working = false;
        });
}

document.addEventListener('DOMContentLoaded', () => {loopImgs()});
